<?php
/**
 * Wordpress recipe
 *
 * This recipe is based on the roots/bedrock Wordpress structure.
 */

namespace Deployer;

require_once 'skape-it-common.php';

// Shared files
$sharedFiles = array(
    '.env',
    'web/robots.txt',
    'web/.htaccess',
);

// Files to exclude in rsync
$excludeFiles = array_merge($sharedFiles, array(
    '.git',
    '.idea',
    'deploy.php',
));

// Shared directories
$sharedDirs = array(
    'web/app/cache',
    'web/app/et-cache',
    'web/app/languages',
    'web/app/uploads',
    'web/app/w3tc-config',
);

// Dirs to exclude in rsync
$excludeDirs = $sharedDirs;

// Set exclude files
set('rsync', array_merge(get('rsync'), array(
    'exclude' => array_merge($excludeFiles, $excludeDirs)
)));

// Shared files/dirs between deploys
add('shared_files', $sharedFiles);
add('shared_dirs', $sharedDirs);
add('writable_dirs', $sharedDirs); // Shared dirs are writable dirs