<?php

/**
 * Magento 2 recipe
 * 
 * This is just deploying the files.
 * You still need to run database and static files upgrades.
 */

namespace Deployer;

require_once 'skape-it-common.php';

set('writable_dirs', [
    'var',
    'pub/static',
    'pub/media',
    'generated'
]);

set('clear_paths', [
    'generated/*',
    'pub/static/_cache/*',
    'var/generation/*',
    'var/cache/*',
    'var/page_cache/*',
    'var/view_preprocessed/*'
]);

// Shared files
$sharedFiles = array(
    'app/etc/env.php',
    'app/etc/config.php',
    'var/.maintenance.ip',
    'pub/.htaccess',
    '.htaccess',
);

// Files to exclude in rsync
$excludeFiles = array_merge($sharedFiles, array(
    'deploy.php',
    '.gitignore',
));

// Shared directories
$sharedDirs = array(
    'var/composer_home',
    'var/log',
    'var/export',
    'var/report',
    'var/import_history',
    'var/session',
    'var/importexport',
    'var/backups',
    'var/tmp',
    'pub/sitemaps',
    'pub/media'
);

// Writable directories
$writableDirs = array(
    'var',
    'pub/static',
    'pub/media',
    'generated'
);

// Dirs to exclude in rsync
$excludeDirs = array_merge($sharedDirs, array(
    '.git',
    '.idea',
));

// Set exclude files
set('rsync', array_merge(get('rsync'), array(
    'exclude' => array_merge($excludeFiles, $excludeDirs)
)));

// Shared files/dirs between deploys
add('shared_files', $sharedFiles);
add('shared_dirs', $sharedDirs);
add('writable_dirs', $writableDirs);