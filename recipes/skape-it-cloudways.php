<?php
/**
 * Cloudways recipe.
 *
 * This recipe is used to restart php-fpm and purge Varnish cache on Cloudways servers.
 */

namespace Deployer;

use Exception;

class CloudwaysAPIClient {
    const API_URL = "https://api.cloudways.com/api/v1";
    private $authKey;
    private $authEmail;
    private $serverId;
    private $accessToken;

    public function __construct($key, $email, $id) {
        $this->authKey   = $key;
        $this->authEmail = $email;
        $this->serverId  = $id;

        $this->prepareAccessToken();
    }

    private function prepareAccessToken() {
        $data = [
            'email'   => $this->authEmail,
            'api_key' => $this->authKey
        ];

        $response = $this->request('POST', '/oauth/access_token', $data);

        $this->accessToken = $response->access_token;
    }

    private function request($method, $url, $post = []) {
        $ch = curl_init();

        if ($ch === false) {
            throw new Exception('failed to initialize CURL');
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_URL, self::API_URL . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        do {
            if ($this->accessToken) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->accessToken]);
            }
            //Set Post Parameters
            $encoded = '';
            if (count($post)) {
                foreach ($post as $name => $value) {
                    $encoded .= urlencode($name) . '=' . urlencode($value) . '&';
                }
                $encoded = substr($encoded, 0, strlen($encoded) - 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
                curl_setopt($ch, CURLOPT_POST, 1);
            }

            $output = curl_exec($ch);

            if ($output === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            # ACCESS TOKEN HAS EXPIRED, so regenerate and retry
            if ($httpcode == '401') {
                $this->prepareAccessToken();
            }
        } while ($httpcode == '401'); // Keep trying if unauthorized

        if ($httpcode == '422') {
            throw new Exception(json_decode($output)->service[0]->message);
        }

        if ($httpcode != '200') {
            throw new Exception('HTTP response ' . $httpcode . ' for ' . $method  . ': ' . $url);
        }
        curl_close($ch);

        return json_decode($output);
    }

    public function restartPhpFpm($phpVersion) {
        if(!$phpVersion) {
            throw new Exception('PHP version variable not set');
        }

        $response = $this->request('POST', '/service/state', [
            'server_id' => $this->serverId,
            'service'   => 'php' . $phpVersion . '-fpm',
            'state'     => 'restart'
        ]);

        if ($response->service_status->status === 'running') {
            return true;
        }

        throw new Exception('Could not restart PHP-FPM on Cloudways. Service ' . $response->service_status->status);
    }

    public function purgeVarnishCache() {
        $response = $this->request('POST', '/service/varnish', [
            'server_id' => $this->serverId,
            'action'     => 'purge'
        ]);

        if ($response->response->status === 'Done') {
            return true;
        }

        throw new Exception('Could not purge Varnish cache. Service ' . $response->service_status->status);
    }
}

/**
 * Restart PHP-FPM on cloudways.
 * Restarting is required after symlinks are changed because symlinks are stored in the PHP-FPM cache.
 */
task('cloudways:php-fpm:restart', function () {
    try {
        $apiClient = new CloudwaysAPIClient(
            get('cloudways_api_key'),
            get('cloudways_api_email'),
            get('cloudways_server_id')
        );

        $apiClient->restartPhpFpm(get('cloudways_php_version'));
    } catch (Exception $e) {
        writeln('<comment>Error while restarting PHP-FPM: ' . $e->getMessage() . '</comment>');
    }
})->desc('Restart php-fpm on Cloudways');

// Restart PHP-FPM on Cloudways.
after('deploy:symlink', 'cloudways:php-fpm:restart');

/**
 * Purge Varnish cache on cloudways.
 */
task('cloudways:varnish:purge', function () {
    try {
        $apiClient = new CloudwaysAPIClient(
            get('cloudways_api_key'),
            get('cloudways_api_email'),
            get('cloudways_server_id')
        );

        $apiClient->purgeVarnishCache();
    } catch (Exception $e) {
        writeln('<comment>Error while purging Varnish cache: ' . $e->getMessage() . '</comment>');
    }
})->desc('Purge Varnish cache on Cloudways');

// Purge varnish cache on Cloudways.
after('deploy:symlink', 'cloudways:varnish:purge');
