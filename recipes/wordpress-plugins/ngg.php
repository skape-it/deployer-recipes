<?php
/**
 * Recipe for the NextGEN gallery Wordpress plugin.
 *
 * This recipe adds the shared NGG directories and is based on the roots/bedrock Wordpress structure.
 */

namespace Deployer;

// Shared directories
$sharedDirs = array(
    'web/app/ngg',
    'web/app/ngg_styles'
);

// Dirs to exclude in rsync
$excludeDirs = $sharedDirs;

// Set exclude files
set('rsync', array_merge(get('rsync'), array(
    'exclude' => array_merge($excludeDirs, get('rsync')['exclude'])
)));

// Shared files/dirs between deploys
add('shared_dirs', $sharedDirs);
add('writable_dirs', $sharedDirs); // Shared dirs are writable dirs