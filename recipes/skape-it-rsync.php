<?php
/**
 * Rsync recipe
 */

namespace Deployer;

// Rsync options
set('rsync', [
    'exclude-file' => false,
    'include'      => [],
    'include-file' => false,
    'flags'        => 'avrz', // Recursive, with compress
    'options'      => ['delete'],
    'timeout'      => 360,
]);

// Set rsync source
set('rsync_src', str_replace('C:\\', '/c/', './'));

// Set rsync destination
set('rsync_dest', '{{release_path}}');

// Set files and directories to exclude
set('rsync_excludes', function () {
    $config        = get('rsync');
    $excludes      = $config['exclude'];
    $excludeFile   = $config['exclude-file'];
    $excludesRsync = '';
    foreach ($excludes as $exclude) {
        $excludesRsync .= ' --exclude=' . escapeshellarg($exclude);
    }
    if ( ! empty($excludeFile) && file_exists($excludeFile) && is_file($excludeFile) && is_readable($excludeFile)) {
        $excludesRsync .= ' --exclude-from=' . escapeshellarg($excludeFile);
    }

    return $excludesRsync;
});

// Set files and directories to include
set('rsync_includes', function () {
    $config        = get('rsync');
    $includes      = $config['include'];
    $includeFile   = $config['include-file'];
    $includesRsync = '';
    foreach ($includes as $include) {
        $includesRsync .= ' --include=' . escapeshellarg($include);
    }
    if ( ! empty($includeFile) && file_exists($includeFile) && is_file($includeFile) && is_readable($includeFile)) {
        $includesRsync .= ' --include-from=' . escapeshellarg($includeFile);
    }

    return $includesRsync;
});

// Set rsync options
set('rsync_options', function () {
    $config       = get('rsync');
    $options      = $config['options'];
    $optionsRsync = [];
    foreach ($options as $option) {
        $optionsRsync[] = "--$option";
    }

    return implode(' ', $optionsRsync);
});

// Set rsync task
task('rsync', function () {
    $config = get('rsync');

    $src = get('rsync_src');
    while (is_callable($src)) {
        $src = $src();
    }

    if ( ! trim($src)) {
        throw new \RuntimeException('You need to specify a source path.');
    }

    $dst = get('rsync_dest');
    while (is_callable($dst)) {
        $dst = $dst();
    }

    if ( ! trim($dst)) {
        // if $dst is not set here we are going to sync to root
        // and even worse - depending on rsync flags and permission -
        // might end up deleting everything we have write permission to
        throw new \RuntimeException('You need to specify a destination path.');
    }

    $server = \Deployer\Task\Context::get()->getHost();
    if ($server instanceof \Deployer\Host\Localhost) {
        runLocally("rsync -{$config['flags']} {{rsync_options}}{{rsync_includes}}{{rsync_excludes}} '$src/' '$dst/'",
            $config);

        return;
    }

    $host         = $server->get('hostname');
    $port         = $server->get('port') ? ' -p' . $server->get('port') : '';
    $sshArguments = $server->getSshArguments();
    $user         = ! $server->get('remote_user') ? '' : $server->get('remote_user') . '@';

    runLocally("rsync -{$config['flags']} -e 'ssh$port $sshArguments' {{rsync_options}}{{rsync_includes}}{{rsync_excludes}} '$src/' '$user$host:$dst/'",
        $config);
});

