<?php
/**
 * Common Skape IT recipe.
 *
 * This recipe is always based on the common Deployer recipe and the Skape IT rsync recipe.
 */

namespace Deployer;

require 'recipe/common.php';
require 'recipe/deploy/cleanup.php';
require 'skape-it-rsync.php';

// Set tty to false and disallow anonymous stats
set('git_tty', false);
set('allow_anonymous_stats', false);
set('use_relative_symlinks', false);
set('ssh_multiplexing', false);

// Set custom release name based on date
set('release_name', function () {
    return date('YmdHis');
});

// Keep small amount of releases
set('keep_releases', 3);

// Tasks
task('skpt:deploy', [
    'deploy:info',
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:shared',
    'deploy:writable',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:cleanup',
    'deploy:success',
])->desc('Deploy your project');