<?php
/**
 * OpenCart recipe
 *
 * This recipe is based on the roots/bedrock Wordpress structure.
 */

namespace Deployer;

require_once 'skape-it-common.php';

// Shared files
$sharedFiles = array(
    '.env',
    'upload/config.php',
    'upload/admin/config.php',
    'upload/vqmod/checked.cache',
    'upload/vqmod/mods.cache'
);

// Files to exclude in rsync
$excludeFiles = array_merge($sharedFiles, array(
    'deploy.php',
    'build.xml',
    '.gitignore',
));

// Shared directories
$sharedDirs = array(
    'storage/cache',
    'storage/download',
    'storage/logs',
    'storage/modification',
    'storage/session',
    'storage/upload',
    'upload/image',
    'upload/vqmod/vqcache',
    'upload/vqmod/logs'
);

// Dirs to exclude in rsync
$excludeDirs = array_merge($sharedDirs, array(
    '.git',
    '.idea',
    'upload/install',
));

// Set exclude files
set('rsync', array_merge(get('rsync'), array(
    'exclude' => array_merge($excludeFiles, $excludeDirs)
)));

// Shared files/dirs between deploys
add('shared_files', $sharedFiles);
add('shared_dirs', $sharedDirs);
add('writable_dirs', $sharedDirs); // Shared dirs are writable dirs