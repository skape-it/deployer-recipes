<?php
/**
 * Laravel recipe.
 */

namespace Deployer;

require_once 'recipe/laravel.php';
require_once 'skape-it-common.php';

// Shared files
$sharedFiles = array(
    '.env',
    'public/.htaccess',
    'public/robots.txt',
);

// Files to exclude in rsync
$excludeFiles = array_merge($sharedFiles, array(
    '.idea',
    '.build',
    'deploy.php',
));

// Shared directories
$sharedDirs = array(
    'storage'
);

// Dirs to exclude in rsync
$excludeDirs = array_merge($sharedDirs, array(
    '.git',
    'node_modules'
));

// Set exclude files
set('rsync', array_merge(get('rsync'), array(
    'exclude' => array_merge($excludeFiles, $excludeDirs)
)));

// Shared files/dirs between deploys
add('shared_files', $sharedFiles);
add('shared_dirs', $sharedDirs);
add('writable_dirs', $sharedDirs); // Shared dirs are writable dirs

// Do laravel tasks
task('laravel', [
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'artisan:migrate',
])->desc('Deploy laravel');

after('deploy:shared', 'laravel');

// Link the storage folder
task('laravel:link:storage', [
    'artisan:storage:link'
]);

after('deploy:symlink', 'laravel:link:storage');
